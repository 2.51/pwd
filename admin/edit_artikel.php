<?php
$id = $_GET['id'];
$query = mysqli_query($conn, "SELECT judul_artikel,id_kategori,deskripsi FROM artikel WHERE id_artikel = '$id'");
list($judul_artikel,$id_kategori,$deskripsi) = mysqli_fetch_array($query);
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Artikel</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Artikel</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-header"><h5>Add Data Artikel <a href="home.php?hal=artikel" class="btn btn-warning btn-sm float-right"><i class="fa fa-arrow-left"></i> Back</a></h5></div>
                        <div class="card-body">
                            <form action="p-edit_artikel.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?=$id?>">
                                <div class="form-group">
                                    <label for="">Judul Artikel <span class="text-danger">*</span></label>
                                    <input type="text" name="judul_artikel" placeholder="Input Judul Artikel" value="<?=$judul_artikel?>" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Kategori <span class="text-danger">*</span></label>
                                    <select name="kategori" class="form-control">
                                        <?php
                                        $query = mysqli_query($conn, "SELECT * FROM kategori");
                                        while($data = mysqli_fetch_array($query))
                                        {
                                        ?>
                                        <option value="<?=$data['id_kategori']?>" <?=($data['id_kategori'] == $id_kategori ? 'selected' : '')?>><?=$data['nama_kategori']?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Thumbnail <span class="text-danger">*</span></label>
                                    <input type="file" name="thumbnail" placeholder="Input Judul Artikel" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Deskripsi <span class="text-danger">*</span></label>
                                    <textarea name="deskripsi" placeholder="Input Judul Artikel" required class="form-control" rows="7"><?=$deskripsi?></textarea>
                                </div>
                                <button class="btn btn-primary"><i class="fa fa-save"></i> Edit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

