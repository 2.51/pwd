<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Kategori</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Kategori</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-header"><h5>Add Data Kategori <a href="home.php?hal=kategori" class="btn btn-warning btn-sm float-right"><i class="fa fa-arrow-left"></i> Back</a></h5></div>
                        <div class="card-body">
                            <form action="p-add_kategori.php" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <label for="">Nama Kategori <span class="text-danger">*</span></label>
                                    <input type="text" name="nama_kategori" placeholder="Input Nama Kategori" required class="form-control">
                                </div>
                                <button class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

