<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Artikel</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Artikel</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header"><h5>Data Artikel <a href="home.php?hal=artikel&act=add" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i> Add</a></h5></div>
                    <div class="card-body">
                            <div class="table-responsive">
                            <table class="table" id="artikel">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Judul Artikel</th>
                                        <th>Thumbnail</th>
                                        <th>Kategori</th>
                                        <th>Tanggal Posting</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $query = mysqli_query($conn, "SELECT id_artikel,judul_artikel,id_kategori,thumbnail,tanggal_posting FROM artikel");
                                    $no = 1;
                                    while($data = mysqli_fetch_array($query))
                                    {
                                        list($nama_kategori) = mysqli_fetch_array(mysqli_query($conn, "SELECT nama_kategori FROM kategori WHERE id_kategori = '".$data['id_kategori']."'"));
                                    ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$data['judul_artikel']?></td>
                                        <td><img src="../img/<?=$data['thumbnail']?>" width="100" alt="<?=$data['judul_artikel']?>"></td>
                                        <td><?=$nama_kategori?></td>
                                        <td><?=$data['tanggal_posting']?></td>
                                        <td><a href="home.php?hal=artikel&act=edit&id=<?=$data['id_artikel']?>" class="btn btn-sm btn-warning"><i class="fa fa-pen"></i> Edit</a> <a onclick="return confirm('Apakah anda yakin ingin menghapus data ini?');" href="p-delete_artikel.php?id=<?=$data['id_artikel']?>" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

