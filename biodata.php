<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body class="biru">
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.html">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="biodata.html">Biodata</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="berita.html">Berita</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="galeri.html">Galeri</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <a href="https://wa.me/6281368330786?text=halo" target="_blank"><img id="wa" src="wa.webp" alt="Whatsapp"></a>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <img src="image.webp" class="img-fluid" alt="Jembatan Ampera">
                </div>
            </div>
        </div>
        <div id="card2" class="col-md-8">
            <div class="card">
                <div class="card-body bg-danger">
                    <div class="table-responsive">
                        <table class="table table-hover table-warning">
                            <thead>
                                <h1 class="text-center">Biodata Diri</h1>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>NPM</td>
                                    <td>:</td>
                                    <td>021110062</td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>Andika Widyanto</td>
                                </tr>
                                <tr>
                                    <td>Jenis Kelamin</td>
                                    <td>:</td>
                                    <td>Pria</td>
                                </tr>
                                <tr>
                                    <td>Tempat Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>Palembang, 21 Desember 1993</td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>:</td>
                                    <td>Palembang</td>
                                </tr>
                                <tr>
                                    <td>No HP</td>
                                    <td>:</td>
                                    <td>0813 6833 0786</td>
                                </tr>
                                <tr>
                                    <td>Hobi</td>
                                    <td>:</td>
                                    <td>
                                        <ul>
                                            <li>Programming</li>
                                            <li>Game</li>
                                            <li>Membaca</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sekolah</td>
                                    <td>:</td>
                                    <td>
                                        <ol>
                                            <li>SD Xaverius 5 Palembang</li>
                                            <li>SMP Xaverius 1 Palembang</li>
                                            <li>SMK Negeri 1 Palembang</li>
                                        </ol>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>